﻿using CloudFlare.API.V4.Data;
using CloudFlare.API.V4.Enums;
using CloudFlare.API.V4.Methods;
using RestSharp;
using System;
using System.Net;
using System.Text;

namespace CloudFlare.API.V4
{
    public class CFProxy
    {

        // Objects to encapsulate API methods
        //public Zones Zones = new Zones();
        //public Methods.Dns Dns = new Methods.Dns();

        #region " Attributes "

        // Config values
        public CFConfig Config = new CFConfig();

        // API Access Configuration
        protected readonly string BaseUrl;
        protected readonly string _apiKey;
        protected readonly string _email;
        protected readonly string zoneName;
        protected readonly string record;

        public CFProxy(CFOptions options = null)
        {
            if (options != null)
            {
                BaseUrl = options.BaseUrl ?? Config.BaseUrl;
                _apiKey = options.ApiKey ?? Config.ApiKey;
                _email = options.Email ?? Config.Email;
                zoneName = options.ZoneName;
                record = options.Record;
            }
            else
            {
                BaseUrl = Config.BaseUrl;
                _apiKey = Config.ApiKey;
                _email = Config.Email;
            }
        }

        #endregion " Attributes "

        #region " Public methods "

        /// <summary>
        /// Executes the request and tries to deserialize the response to T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// 9/5/2013 by Sergi
        public T Execute<T>(RestRequest request, string method) where T : new()
        {
            var client = new RestClient
            {
                BaseUrl = new Uri(BaseUrl + method)
            };

            client.AddDefaultHeader(CFHeaders.ApiKey, _apiKey);
            client.AddDefaultHeader(CFHeaders.Email, _email);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute<T>(request);

            // Checking call success
            HandleRequestError(response);
            HandleResponseError(response.Data as ResponseBase);

            return response.Data;
        }

        #endregion " Public methods "

        #region " Private methods "

        /// <summary>
        /// Handles errors in the request (Network, transport, etc.)
        /// </summary>
        /// <param name="response">The response.</param>
        /// 9/6/2013 by Sergi
        private void HandleRequestError(IRestResponse response)
        {
            bool success = true;
            string message = "";
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                message = String.Format("Network error. Response status: {0}. Error: {1}", response.ResponseStatus.ToString(), response.ErrorMessage);
                success = false;

            }
            if (response.StatusCode != HttpStatusCode.OK)
            {
                message = String.Format("Server error. HTTP Status code: {0}. Error: {1}", response.StatusCode.ToString(), response.Content);
                success = false;
            }
            if (response.ErrorException != null)
            {
                message = "Error retrieving response. Check inner details for more info.";
                success = false;
            }
            if (success) return;

            var proxyException = new ApplicationException(message, response.ErrorException);
            throw proxyException;
        }

        /// <summary>
        /// Handles an error in the remote service.
        /// </summary>
        /// <param name="response">The response.</param>
        /// 9/6/2013 by Sergi
        private static void HandleResponseError(ResponseBase response)
        {
            if (response.Success.HasValue && response.Success.Value) return;

            StringBuilder sb = new StringBuilder();
            foreach (var e in response.Errors)
            {
                sb.AppendLine(e.Code + ": " + e.Message);
            }
            var proxyException = new ApplicationException(sb.ToString());
            throw proxyException;
        }

        #endregion " Private methods "

    }
}
