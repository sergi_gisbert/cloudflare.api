﻿using CloudFlare.API.V4.Data;
using CloudFlare.API.V4.Data.Results;
using CloudFlare.API.V4.Enums;
using RestSharp;
using System.Linq;

namespace CloudFlare.API.V4.Methods
{
    public class Zones : CFProxy
    {
        #region " Attributes "

        /// <summary>
        /// The request
        /// </summary>
        private static readonly RestRequest request = new RestRequest(Method.POST);

        public Zones(CFOptions options = null) : base(options)
        {
        }

        #endregion " Attributes "

        #region " Public methods "
        public ZoneResult[] GetZones(string name = "", int page = 1, int pageSize = 50)
        {
            var method = "zones";
            if (string.IsNullOrEmpty(name))
                name = this.zoneName;

            request.AddParameter(CFParameters.Page, page);
            request.AddParameter(CFParameters.PageSize, pageSize);
            if (!string.IsNullOrEmpty(name))
                request.AddParameter(CFParameters.Name, name);

            request.Method = Method.GET;

            var response = Execute<SimpleResponse>(request, method);
            return ZoneResult.FromJson(response.Result.ToString());
        }

        public ZoneResult GetZone(string name)
        {
            if (string.IsNullOrEmpty(name))
                name = this.zoneName;

            var zones = GetZones(name);
            return zones.Any() ? zones.FirstOrDefault() : null;
        }
        #endregion 
    }
}
