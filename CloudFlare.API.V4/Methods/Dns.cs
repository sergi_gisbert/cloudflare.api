﻿using CloudFlare.API.V4.Data;
using CloudFlare.API.V4.Data.Results;
using CloudFlare.API.V4.Enums;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudFlare.API.V4.Methods
{
    public class Dns : CFProxy
    {
        #region " Attributes "

        /// <summary>
        /// The request
        /// </summary>
        private static readonly RestRequest request = new RestRequest(Method.POST);

        public Dns(CFOptions options = null) : base(options)
        {
        }

        #endregion " Attributes "

        #region " Public methods "

        public DnsResult[] GetDnsRecords(string zoneName, string name = "", int page = 1, int pageSize = 50)
        {
            var method = "zones/{0}/dns_records";
            var zoneId = GetZoneId(zoneName);
            if (!string.IsNullOrEmpty(zoneId))
            {
                method = string.Format(method, zoneId);

                request.AddParameter(CFParameters.Page, page);
                request.AddParameter(CFParameters.PageSize, pageSize);
                if (string.IsNullOrEmpty(name))
                    name = this.record;

                if (!string.IsNullOrEmpty(name))
                    request.AddParameter(CFParameters.Name, name + "." + zoneName);
                request.Method = Method.GET;

                var response = Execute<SimpleResponse>(request, method);
                return DnsResult.FromJson(response.Result.ToString());
            }
            else
            {
                var proxyException = new ApplicationException("No Zone was found with given name: " + zoneName);
                throw proxyException;
            }
        }

        public DnsResult GetDnsRecord(string zoneName, string name)
        {
            var records = GetDnsRecords(zoneName, name);
            return records.Any() ? records.FirstOrDefault() : null;
        }

        public bool EnableDnsRecord(string zoneName, string name, bool enable = true)
        {
            var method = "zones/{0}/dns_records/{1}";
            var zoneId = GetZoneId(zoneName);
            if (!string.IsNullOrEmpty(zoneId))
            {
                var record = GetDnsRecord(zoneName, name);
                if (record != null)
                {
                    method = string.Format(method, zoneId, record.Id);
                    record.Proxied = enable;

                    RestRequest request = new RestRequest(Method.PUT);
                    ShortDnsRecord updatedRecord = new ShortDnsRecord(record);
                    request.RequestFormat = DataFormat.Json;
                    request.AddParameter("application/json; charset=utf-8", updatedRecord.ToJson(), ParameterType.RequestBody);

                    var response = Execute<SimpleResponse>(request, method);
                    return response.Success.HasValue && response.Success.Value;
                }
            }
            return false;
        }

        public bool DisableDnsRecord(string zoneName, string name)
        {
            return EnableDnsRecord(zoneName, name, false);
        }

        #endregion

        #region " Private methods "

        private string GetZoneId(string zoneName)
        {
            var CFOptions = new CFOptions(this._apiKey, this._email, this.BaseUrl, this.zoneName, this.record);
            Zones zonesProxy = new Zones(CFOptions);
            var zone = zonesProxy.GetZone(zoneName);
            return (zone != null) ? zone.Id : string.Empty;
        }

        #endregion
    }

}
