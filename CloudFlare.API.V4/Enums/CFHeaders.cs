﻿namespace CloudFlare.API.V4.Enums
{
    /// <summary>
    /// Strings corresponding to the method's parameters
    /// </summary>
    /// 9/13/2013 by Sergi
    internal struct CFHeaders
    {
        public const string ApiKey = "X-Auth-Key";
        public const string Email = "X-Auth-Email";


        public const string Method = "a";
        public const string Domain = "z";
        public const string Interval = "interval";
        public const string Zones = "zones";
        public const string Hours = "hours";
        public const string Class = "class";
        public const string Geo = "geo";
        public const string IP = "ip";
        public const string Type = "type";
        public const string Name = "name";
        public const string Content = "content";
        public const string TTL = "ttl";
        public const string Id = "id";
        public const string ServiceMode = "service_mode";
        public const string DevMode = "v";
        public const string PurgeCache = "v";
        public const string Url = "url";
        public const string ZoneId = "zid";
        public const string Key = "key";
        public const string V = "v";
    }
}
