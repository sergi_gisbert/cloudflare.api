﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace CloudFlare.API.V4.Data
{
    /// <summary>
    /// Class for mapping the common result messages for all the requests
    /// </summary>
    public abstract class ResponseBase
    {
        public static ResponseBase FromJson(string json) => JsonConvert.DeserializeObject<ResponseBase>(json, CloudFlare.API.V4.Data.Converter.Settings);

        [JsonProperty("result", NullValueHandling = NullValueHandling.Ignore)]
        public Object Result { get; set; }

        [JsonProperty("success", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Success { get; set; }

        [JsonProperty("result_info", NullValueHandling = NullValueHandling.Ignore)]
        public ResultInfo ResultInfo { get; set; }

        [JsonProperty("errors", NullValueHandling = NullValueHandling.Ignore)]
        public List<ErrorElement> Errors { get; set; }

        [JsonProperty("messages", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Messages { get; set; }

    }

    public partial class ErrorElement
    {
        [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
        public long? Code { get; set; }

        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }
    }

    public partial class ResultInfo
    {
        [JsonProperty("page", NullValueHandling = NullValueHandling.Ignore)]
        public long? Page { get; set; }

        [JsonProperty("per_page", NullValueHandling = NullValueHandling.Ignore)]
        public long? PerPage { get; set; }

        [JsonProperty("count", NullValueHandling = NullValueHandling.Ignore)]
        public long? Count { get; set; }

        [JsonProperty("total_count", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalCount { get; set; }
    }

    public static class Serialize
    {
        public static string ToJson(this ResponseBase self) => JsonConvert.SerializeObject(self, CloudFlare.API.V4.Data.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    public class SimpleResponse : ResponseBase
    {
        public SimpleResponse()
        {
        }
    }
}
