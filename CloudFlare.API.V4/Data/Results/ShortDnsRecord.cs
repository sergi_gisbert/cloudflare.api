﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudFlare.API.V4.Data.Results
{
    public class ShortDnsRecord
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("content", NullValueHandling = NullValueHandling.Ignore)]
        public string Content { get; set; }

        [JsonProperty("proxied", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Proxied { get; set; }

        [JsonProperty("ttl", NullValueHandling = NullValueHandling.Ignore)]
        public long? Ttl { get; set; }

        public ShortDnsRecord(DnsResult result)
        {
            Type = result.Type;
            Name = result.Name;
            Content = result.Content;
            Proxied = result.Proxied;
            Ttl = result.Ttl;
        }
    }

    public static class ShortDnsResultSerializer
    {
        public static string ToJson(this ShortDnsRecord self) => JsonConvert.SerializeObject(self, CloudFlare.API.V4.Data.Results.Converter.Settings);
    }
}
