﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console.CommandLineOptions
{
    public class OptionsBase
    {
        [Option('k', "apikey", Required = false, HelpText = "Your CloudFlare api key")]
        public string ApiKey { get; set; }

        [Option('e', "email", Required = false, HelpText = "Your CloudFlare Email")]
        public string Email { get; set; }

        [Option('a', "api", Required = false, HelpText = "CloudFlare API endpoint URL", Default = "https://api.cloudflare.com/client/v4/")]
        public string BaseUrl { get; set; }
    }
}
