﻿using CommandLine;

namespace Console.CommandLineOptions
{
    public enum RecordEnum
    {
        EnableRecord,
        DisableRecord,
        GetRecord,
        GetRecords
    }

    [Verb("record", HelpText = "Manage Records")]
    public class RecordOptions : OptionsBase
    {
        [Option('c', "command", Required = true, HelpText = "Action to perform with the record. Options are: 'EnableRecord', 'DisableRecord', 'GetRecord'", Default = RecordEnum.EnableRecord )]
        public RecordEnum Action { get; set; }

        [Option('r', "recordName", HelpText = "DNS Record name")]
        public string Record { get; set; }

        [Option('z', "zoneName", Required = false, HelpText = "Zone name")]
        public string ZoneName { get; set; }
    }
}
