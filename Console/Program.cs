﻿using System;
using CloudFlare.API.V4;
using CloudFlare.API.V4.Data;
using CloudFlare.API.V4.Methods;
using CommandLine;
using Console.CommandLineOptions;

namespace Console
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Parser.Default.ParseArguments<ZoneOptions, RecordOptions>(args)
                    .MapResult(
                        (ZoneOptions opts) => ManageZoneActions(opts),
                        (RecordOptions opts) => ManageRecordActions(opts),
                        errs => 1
                    );
            }
            catch (ApplicationException ex)
            {
                var error = String.Format("ERROR: {0}", ex.Message);
                if (ex.InnerException != null)
                    error += String.Format("\nInner exception: {0}", ex.InnerException.Message);

                System.Console.WriteLine(error);
            }
            //System.Console.ReadLine();

            return;
        }

        private static int ManageZoneActions(ZoneOptions o)
        {
            var CFOptions = new CFOptions(o.ApiKey, o.Email, o.BaseUrl, o.ZoneName);
            var Zones = new Zones(CFOptions);
            switch (o.Action)
            {
                case ZoneEnum.GetZone:
                    if (string.IsNullOrEmpty(CFOptions.ZoneName))
                    {
                        System.Console.WriteLine("GET ZONE: -z parameter with 'ZoneName' is required");
                        return (int)ErrorType.MissingRequiredOptionError;
                    }
                    var zone = Zones.GetZone(CFOptions.ZoneName);
                    if (zone != null)
                        System.Console.WriteLine("{0} - {1} ({2})", zone.Id, zone.Name, zone.Status);

                    break;

                case ZoneEnum.GetZones:
                    //    RETRIEVE COMPLETE ZONES LIST
                    System.Console.WriteLine("Zones LIST\n----------------------------------------------");
                    var zones = Zones.GetZones();
                    foreach (var z in zones)
                    {
                        System.Console.WriteLine("{0} - {1} ({2})", z.Id, z.Name, z.Status);
                    }
                    System.Console.WriteLine("");
                    break;

                default:
                    System.Console.WriteLine("MANAGE ZONES:  invalid command -c parameter");
                    return (int)ErrorType.MissingRequiredOptionError;
            }
            return 0;
        }

        private static int ManageRecordActions(RecordOptions o)
        {
            var CFOptions = new CFOptions(o.ApiKey, o.Email, o.BaseUrl, o.ZoneName, o.Record);
            Dns Dns = new Dns(CFOptions);
            switch (o.Action)
            {
                case RecordEnum.GetRecords:
                    var records = Dns.GetDnsRecords(CFOptions.ZoneName);
                    foreach (var r1 in records)
                        System.Console.WriteLine("   - {0} - {1} - {2}", r1.Type, r1.Name, r1.Content);
                    break;

                case RecordEnum.GetRecord:
                    if (string.IsNullOrEmpty(CFOptions.Record))
                    {
                        System.Console.WriteLine("GET Record: -r parameter with 'RecordName' is required");
                        return (int)ErrorType.MissingRequiredOptionError;
                    }
                    var r = Dns.GetDnsRecord(CFOptions.ZoneName, CFOptions.Record);
                    if(r != null)
                        System.Console.WriteLine("{0} - {1} - {2}", r.Type, r.Name, r.Content);

                    break;
                case RecordEnum.EnableRecord:
                case RecordEnum.DisableRecord:
                    if (string.IsNullOrEmpty(CFOptions.Record))
                    {
                        System.Console.WriteLine("GET Record: -r parameter with 'RecordName' is required");
                        return (int)ErrorType.MissingRequiredOptionError;
                    }
                    bool result = false;
                    switch (o.Action)
                    {
                        case RecordEnum.EnableRecord:
                            result = Dns.EnableDnsRecord(CFOptions.ZoneName, CFOptions.Record);
                            break;
                        case RecordEnum.DisableRecord:
                            result = Dns.DisableDnsRecord(CFOptions.ZoneName, CFOptions.Record);
                            break;
                    }
                    if (result)
                    {
                        var modified = Dns.GetDnsRecord(CFOptions.ZoneName, CFOptions.Record);
                        if (modified != null)
                        {
                            System.Console.WriteLine("'{0}' enabled -> {1}", modified.Name, modified.Proxied);
                        }
                    }
                    break;

                default:
                    System.Console.WriteLine("MANAGE RECORDS:  invalid command -c parameter");
                    return (int)ErrorType.MissingRequiredOptionError;
            }
            return 0;
        }
    }
}


