﻿using CloudFlare.API.Utils;

namespace CloudFlare.API.Enums
{
    /// <summary>
    /// Rocket Loader Modes
    /// </summary>
    public enum RocketLoaderMode
    {
        [StringValue("0")]
        Off,

        [StringValue("a")]
        Automatic,

        [StringValue("m")]
        Manual
    }
}
