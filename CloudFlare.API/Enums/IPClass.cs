﻿using CloudFlare.API.Utils;

namespace CloudFlare.API.Enums
{
    /// <summary>
    /// Status for the developer mode
    /// </summary>
    /// 9/7/2013 by Sergi
    public enum IPClass
    {
        [StringValue("r")]
        Regular,

        [StringValue("s")]
        Crawler,

        [StringValue("t")]
        Threat,

        [StringValue("all")]
        All
    }
}
