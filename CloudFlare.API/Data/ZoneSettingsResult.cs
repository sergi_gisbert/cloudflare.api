﻿using System;
using System.Collections.Generic;

namespace CloudFlare.API.Data
{
    /// <summary>
    /// Class for mapping the result messages for Domains related requests
    /// The hierarchy is:
    ///     - request (from ResponseBase)
    ///     - result (from ResponseBase)
    ///     - msg (from ResponseBase)
    ///     - response: the response message
    ///         - zones: {Zones}
    ///             
    /// </summary>
    public class ZoneSettingsResponse : ResponseBase
    {
        public ZoneSettingsResume response { get; set; }
    }

    public class ZoneSettingsResume
    {
        public ZoneSettingsResult result { get; set; }
    }

    public class ZoneSettingsResult
    {
        public List<ZoneSettings> objs { get; set; }
    }

    public class ZoneSettings
    {
        /// <summary>
        /// The user security setting level.
        /// </summary>
        /// <value>
        /// The user security setting.
        /// </value>
        /// 9/8/2013 by Sergi
        public string userSecuritySetting { get; set; }

        /// <summary>
        /// Development Mode status. A zero response indicates off. A non-zero response indicates on. 
        /// The numerical value of a non-zero response indicates when Development Mode will expire.
        /// </summary>
        /// <value>
        /// The dev_mode.
        /// </value>
        /// 9/8/2013 by Sergi
        public string dev_mode { get; set; }

        /// <summary>
        /// IPV6, Values are [0 = off, 3 = Full]
        /// </summary>
        /// <value>
        /// The ipv46.
        /// </value>
        /// 9/8/2013 by Sergi
        public string ipv46 { get; set; }

        /// <summary>
        ///  Always Online status; Values are [0 = off; 1 = on].
        /// </summary>
        /// <value>
        /// The object.
        /// </value>
        /// 9/8/2013 by Sergi
        public string ob { get; set; }

        /// <summary>
        /// Caching Level. Values are: [agg = aggressive, iqs = simplified, basic]
        /// </summary>
        /// <value>
        /// The cache_lvl.
        /// </value>
        /// 9/8/2013 by Sergi
        public string cache_lvl { get; set; }

        public string outboundLinks { get; set; }

        /// <summary>
        /// Rocket Loader. Values are: [0 = off, a = automatic, m = manual.]
        /// </summary>
        /// <value>
        /// The asynchronous.
        /// </value>
        /// 9/8/2013 by Sergi
        public string async { get; set; }

        /// <summary>
        /// Browser Integrity Check. Values are [0 = off, 1 = on]
        /// </summary>
        /// <value>
        /// The bic.
        /// </value>
        /// 9/8/2013 by Sergi
        public string bic { get; set; }

        /// <summary>
        /// Challenge TTL, in seconds.
        /// </summary>
        /// <value>
        /// The CHL_TTL.
        /// </value>
        /// 9/8/2013 by Sergi
        public string chl_ttl { get; set; }

        /// <summary>
        /// Expire TTL (for CloudFlare-cached items), in seconds.
        /// </summary>
        /// <value>
        /// The exp_ttl.
        /// </value>
        /// 9/8/2013 by Sergi
        public string exp_ttl { get; set; }

        public string fpurge_ts { get; set; }

        /// <summary>
        /// Hotlink Protection. Values are [0 = off, 1 = on]
        /// </summary>
        /// <value>
        /// The hotlink.
        /// </value>
        /// 9/8/2013 by Sergi
        public string hotlink { get; set; }

        /// <summary>
        /// [PRO/BUSINESS/ENTERPRISE] Mirage: Auto-resize, Polish settings
        /// 0 = Off
        /// 1 = Auto-resize on
        /// 200 = Polish: Basic
        /// 201 = Polish: Basic, Mirage: Auto-resize on
        /// 170 = Polish: Basic + JPEG
        /// 171 = Polish: Basic + JPEG, Mirage: Auto-resize on
        /// </summary>
        /// <value>
        /// The img.
        /// </value>
        /// 9/8/2013 by Sergi
        public string img { get; set; }

        /// <summary>
        /// [PRO/BUSINESS/ENTERPRISE] Mirage: Lazy Load. Values are [0 = off, 1 = on]
        /// </summary>
        /// <value>
        /// The lazy.
        /// </value>
        /// 9/8/2013 by Sergi
        public string lazy { get; set; }

        /// <summary>
        /// Minification
        /// 0 = off
        /// 1 = JavaScript only
        /// 2 = CSS only
        /// 3 = JavaScript and CSS
        /// 4 = HTML only
        /// 5 = JavaScript and HTML
        /// 6 = CSS and HTML
        /// 7 = CSS, JavaScript, and HTML
        /// </summary>
        /// <value>
        /// The minify.
        /// </value>
        /// 9/8/2013 by Sergi
        public string minify { get; set; }
        public string outlink { get; set; }

        /// <summary>
        /// [PRO/BUSINESS/ENTERPRISE] Preloader. Values are [0 = off, 1 = on]
        /// </summary>
        /// <value>
        /// The preload.
        /// </value>
        /// 9/8/2013 by Sergi
        public string preload { get; set; }

        public string s404 { get; set; }

        /// <summary>
        /// Basic Security Level. Values are [eoff (Essentially Off), low, med, high, help (I'm Under Attack!)]
        /// </summary>
        /// <value>
        /// The sec_lvl.
        /// </value>
        /// 9/8/2013 by Sergi
        public string sec_lvl { get; set; }

        /// <summary>
        /// [PRO/BUSINESS/ENTERPRISE] SPDY. Values are [0 = off, 1 = on]
        /// </summary>
        /// <value>
        /// The spdy.
        /// </value>
        /// 9/8/2013 by Sergi
        public string spdy { get; set; }

        /// <summary>
        /// [PRO/BUSINESS/ENTERPRISE] SSL Status. Values are [0 = off, 1 = Flexible, 2 = Full]
        /// </summary>
        /// <value>
        /// The SSL.
        /// </value>
        /// 9/8/2013 by Sergi
        public string ssl { get; set; }

        /// <summary>
        /// [PRO/BUSINESS/ENTERPRISE] WAF setting. Values are [high, low, off]
        /// </summary>
        /// <value>
        /// The waf_profile.
        /// </value>
        /// 9/8/2013 by Sergi
        public string waf_profile { get; set; }

        public string host_spf { get; set; }
        public string email_filter { get; set; }
        public string sse { get; set; }
        public string cache_ttl { get; set; }
        public string ddos { get; set; }
    }
}
