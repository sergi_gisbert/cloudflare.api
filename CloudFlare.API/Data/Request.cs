﻿namespace CloudFlare.API.Data
{
    /// <summary>
    /// Class for mapping the Request
    /// </summary>
    public class Request
    {
        public string act { get; set; }
        public string a { get; set; }       // The method to execute
        public string tkn { get; set; }     // The API key
        public string email { get; set; }   // The email associated to the API key
        public string type { get; set; }
        public string z { get; set; }
        public string name { get; set; }
        public string content { get; set; }
        public string ttl { get; set; }
        public string service_mode { get; set; }
    }
}
