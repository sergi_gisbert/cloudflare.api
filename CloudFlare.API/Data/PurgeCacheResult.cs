﻿using System;
using System.Collections.Generic;

namespace CloudFlare.API.Data
{
    /// <summary>
    /// Class for mapping the result messages for Purge cache related request
    /// The hierarchy is:
    ///     - request (from ResponseBase)
    ///     - result (from ResponseBase)
    ///     - msg (from ResponseBase)
    ///     - response: the response message
    ///         - zone: {Zone}
    ///             - obj: {ZoneObject}
    ///             
    /// </summary>
    public class PurgeCacheResponse : ResponseBase
    {
        public PurgeCacheResult response { get; set; }
        public AttributesResult attributes { get; set; }
    }

    public class SingleFilePurgeCacheResponse : ResponseBase
    {
        public SingleFilePurgeCacheResult response { get; set; }
    }

    public class AttributesResult
    {
        public int cooldown { get; set; }
    }

    public class PurgeCacheResult
    {
        public string fpurge_ts { get; set; }
    }

    public class SingleFilePurgeCacheResult
    {
        public string vtxt_match { get; set; }
        public string url { get; set; }
    }
}
