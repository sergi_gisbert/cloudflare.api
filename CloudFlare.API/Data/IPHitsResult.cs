﻿using System;
using System.Collections.Generic;

namespace CloudFlare.API.Data
{
    /// <summary>
    /// Class for mapping the result messages for Purge cache related request
    /// The hierarchy is:
    ///     - request (from ResponseBase)
    ///     - result (from ResponseBase)
    ///     - msg (from ResponseBase)
    ///     - response: the response message
    ///         - zone: {Zone}
    ///             - obj: {ZoneObject}
    ///             
    /// </summary>
    public class IPHitsResponse : ResponseBase
    {
        public IPHitsResume response { get; set; }
    }

    public class IPHitsResume
    {
        public List<IPHitsResult> ips { get; set; }
    }

    public class IPHitsResult
    {
        public string ip { get; set; }
        public string classification { get; set; }
        public string hits { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string zone_name { get; set; }
    }
}
