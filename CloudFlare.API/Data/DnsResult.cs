﻿using System;
using System.Collections.Generic;

namespace CloudFlare.API.Data
{
    /// <summary>
    /// Class for mapping the result messages for DNS related requests
    /// The hierarchy is:
    ///     - request (from ResponseBase)
    ///     - result (from ResponseBase)
    ///     - msg (from ResponseBase)
    ///     - response: the response message
    ///         - rec
    ///             - obj: the DnsObject
    /// </summary>
    public class DnsResponse : ResponseBase
    {
        public DnsResult response { get; set; }
    }

    public class DnsResult
    {
        public DnsRecord rec { get; set; }
    }

    public class DnsRecord
    {
        public DnsObject obj { get; set; }
    }

    /// <summary>
    /// Class for mapping the result messages for DNS records related requests
    /// The hierarchy is:
    ///     - request (from ResponseBase)
    ///     - result (from ResponseBase)
    ///     - msg (from ResponseBase)
    ///     - response: the response message
    ///         - recs
    ///             - count
    ///             - has_more
    ///             - objs : List{DnsObject}
    /// </summary>
    public class DnsRecordsResponse : ResponseBase
    {
        public DnsRecs response { get; set; }
    }

    public class DnsRecs
    {
        public DnsRecordsResume recs { get; set; }
    }

    public class DnsRecordsResume
    {
        public Int32 count { get; set; }
        public Boolean has_more { get; set; }
        public List<DnsObject> objs { get; set; }
    }

    /// <summary>
    /// Data related to a DNS Record
    /// </summary>
    public class DnsObject
    {
        public string rec_id { get; set; }
        public string rec_tag { get; set; }
        public string zone_name { get; set; }
        public string name { get; set; }
        public string display_name { get; set; }
        public string type { get; set; }
        public string prio { get; set; }
        public string content { get; set; }
        public string display_content { get; set; }
        public string ttl { get; set; }
        public string ttl_ceil { get; set; }
        public string ssl_id { get; set; }
        public string ssl_status { get; set; }
        public string ssl_expires_on { get; set; }
        public string auto_ttl { get; set; }
        public string service_mode { get; set; }
        public DnsProps props { get; set; }
    }

    /// <summary>
    /// Properties from the DNS Record
    /// </summary>
    public class DnsProps
    {
        public int proxiable { get; set; }
        public int cloud_on { get; set; }
        public int cf_open { get; set; }
        public int ssl { get; set; }
        public int expired_ssl { get; set; }
        public int expiring_ssl { get; set; }
        public int pending_ssl { get; set; }
        public int vanity_lock { get; set; }
    }
}
