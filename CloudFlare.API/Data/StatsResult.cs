﻿using System;
using System.Collections.Generic;

namespace CloudFlare.API.Data
{
    /// <summary>
    /// Class for mapping the result messages for Statistics related requests
    /// The hierarchy is:
    ///     - request (from ResponseBase)
    ///     - result (from ResponseBase)
    ///     - msg (from ResponseBase)
    ///     - response: the response message
    ///         - result
    ///             - timeZero
    ///             - timeEnd
    ///             - count
    ///             - has_more
    ///             - objs: List{StatsObject}
    /// </summary>
    public class StatsResponse : ResponseBase
    {
        public StatsResult response { get; set; }
    }

    public class StatsResult
    {
        public StatsResume result { get; set; }
    }
    public class StatsResume
    {
        public String timeZero { get; set; }
        public String timeEnd { get; set; }
        public Int32 count { get; set; }
        public Boolean has_more { get; set; }
        public List<StatsObject> objs { get; set; }
    }

    /// <summary>
    /// Statistic data for each domain
    /// </summary>
    public class StatsObject
    {
        public string cachedServerTime { get; set; }
        public string cachedExpryTime { get; set; }
        public TrafficBreakdown trafficBreakdown { get; set; }
        public Traffic bandwidthServed { get; set; }
        public Traffic requestsServed { get; set; }
        public bool pro_zone { get; set; }
        public string pageLoadTime { get; set; }
        public string currentServerTime { get; set; }
        public int interval { get; set; }
        public string zoneCDate { get; set; }
        public string userSecuritySetting { get; set; }
        public int dev_mode { get; set; }
        public int ipv46 { get; set; }
        public int ob { get; set; }
        public string cache_lvl { get; set; }
    }

    public class Traffic
    {
        public float cloudflare { get; set; }
        public float user { get; set; }
    }

    public class TrafficBreakdown
    {
        public Views pageviews { get; set; }
        public Views uniques { get; set; }
    }

    public class Views
    {
        public float regular { get; set; }
        public float threat { get; set; }
        public float crawler { get; set; }
    }
}
