﻿using System;
using System.Collections.Generic;

namespace CloudFlare.API.Data
{
    /// <summary>
    /// Class for mapping the result messages for Purge cache related request
    /// The hierarchy is:
    ///     - request (from ResponseBase)
    ///     - result (from ResponseBase)
    ///     - msg (from ResponseBase)
    ///     - response: the response message
    ///         - zone: {Zone}
    ///             - obj: {ZoneObject}
    ///             
    /// </summary>
    public class IPRestrictionResponse : ResponseBase
    {
        public IPRestrictionResult response { get; set; }
    }

    public class IPRestrictionResult
    {
        public IPRestriction result { get; set; }
    }

    public class IPRestriction
    {
        public string ip { get; set; }
        public string action { get; set; }
    }
}
